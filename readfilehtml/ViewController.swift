//
//  ViewController.swift
//  readfilehtml
//
//  Created by Nguyen Tien Duc  on 1/14/21.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webview: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fileMgr = FileManager.default
//        let curentPath = fileMgr.currentDirectoryPath
//        print("curentPath: \(curentPath)")
        
        let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
//        print("dirPaths size: \(dirPaths.count)")
        let docsDir = dirPaths[0].path
//        print("docsDir: \(docsDir)")
        
        var filename = ""
        do {
            let fileList = try fileMgr.contentsOfDirectory(atPath: docsDir)
            
            for f in fileList {
                if f.starts(with: "indexios") {
                    filename = f
                    break
                }
            }
        } catch let err {
            print(err.localizedDescription)
        }
        let path = "\(docsDir)/\(filename)"
//        print(path)
        var html = ""
        do {
            try html = String(contentsOfFile: path, encoding: .utf8)
        } catch {
        //ERROR
        }
        let urlstr = "https://trade.vndirect.com.vn"
//        let urlstr = "https://trading.entrade.com.vn"
        webview.loadHTMLString(html, baseURL: URL(string: urlstr)!)
        
        UIApplication.shared.isIdleTimerDisabled = true
    }
    

}

